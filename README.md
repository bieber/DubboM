#DubboM——Dubbo Micro

##目的
通过利用`Dubbo`的扩展性以及RPC的模型开发一个满足微服务的要求，通过构建一个RPC的环境，让应用在不依赖外部第三方容器能够进行服务的发布和调用，从而打到应用的隔离以及完整性。应用应该只需要关注与它的服务实现以及服务的调用，而不需要关注服务在处于什么样的一个环境，基于这个便构建了`DubboM(Dubbo Micro)`。
##和其他的RPC框架有什么不同
可以说`DubboM`是基于RPC框架而开发的，是一次进一步的升华。由于企业需要用的不只是一个进行具备RPC功能的框架，而是需要一个基于RPC可实施的方案。框架本身可以理解为一个种工具，而方案则是解决某些业务场景。那么`DubboM`是解决什么场景呢？那就是微服务。微服务可以理解是一种拆分，一种解耦，一种更细致的服务化途径，同时也是让应用更加轻巧。`DubboM`提供微服务运行需要的RPC通信环境，并且不依赖外部传统的servlet容器，从而让应用更加清爽。
##项目获取
如果你还不熟悉git，或者不知道git是什么，请百度，根据相关文档完成git安装以及基本使用

通过一下命令获取`DubboM`源码：

`git clone https://git.oschina.net/bieber/DubboM.git`

##构建项目

此处假设你通过上面的`git`命令把源码下载到本地的`/git/DubboM`下（后面均以`${DubboM_Source_Home}`）.

> `cd ${DubboM_Source_Home}`

到该目录下面,执行如下目录

> `./install-${rpc_type}.bat(sh)`

其中`${rpc_type}`分别是`dubbo`，`restful`,`dubbo-netty4`和`all`

主要是围绕两种协议来做打包，分别是`dubbo`和`restful`，而`all`是同时打一个包含`dubbo`和`restful`协议的包，并且此时`dubbo`协议是通过`netty4`来实现，`dubbo-netty4`是打出netty4实现的`dubbo`协议。

执行完上面脚本之后，在`${DubboM_Source_Home}/target`下面会有`DubboM-launcher`目录，该目录下有如下几个文件夹：

`bin`:启动该DubboM-launcher脚本

`conf`:该应用相关的配置，会加载到`classpath`下面

`libs`:DubboM依赖的相关jar文件

`apps`:该DubboM-launcher负责启动的dubbom应用，将该应用的jar以及相关依赖的jar添加到该目录下

###什么是DubboM-launcher

你可能会问什么是`DubboM-launcher`,`DubboM-launcher`可以理解为是一个DubboM应用的启动器，可以简单理解为和servlet容器的tomcat或者jetty类似，但是唯一不同的是tomcat可以管理多个应用实体，但是`DubboM-launcher`有且只能有一个应用，只要你把你的应用jar拷贝到apps目录下，你可以把`DubboM-launcer`
拷贝到任何地方，任何物理节点，或者虚机或者是docker，因为它已经具备了该应用运行的完整环境。

###直接获取DubboM-launcher

从下面链接可以下载对应不同版本的launcher

[DubboM-launcher同时支持restful和dubbo协议版本](http://git.oschina.net/bieber/DubboM/attach_files/download?i=30093&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F00%2FCC%2FZxV3aVcO9lOAXDi6AMQB-9KROtU2644.gz%3Ftoken%3D81ebf24ed62d6246d0edf3c947783992%26ts%3D1460598389%26attname%3DDubboM-launcher-all.tar.gz)

[DubboM-launcher支持dubbo协议（netty3）版本](http://git.oschina.net/bieber/DubboM/attach_files/download?i=30094&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F00%2FCC%2FZxV3aVcO9oaAOtO-AJnn3d6a2Ro3551.gz%3Ftoken%3D1c036874c7cb83530c9e20d763710c36%26ts%3D1460598389%26attname%3DDubboM-launcher-dubbo-netty3.tar.gz)

[DubboM-launcher支持dubbo协议(netty4)版本](http://git.oschina.net/bieber/DubboM/attach_files/download?i=30095&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F00%2FCC%2FZxV3aVcO9pWAPnU_AKPrI0I1T-M3081.gz%3Ftoken%3D9ac07b5999f8bc8e6e06cd27d79960a2%26ts%3D1460598389%26attname%3DDubboM-launcher-dubbo-netty4.tar.gz)

[DubboM-launcher支持restful协议版本](http://git.oschina.net/bieber/DubboM/attach_files/download?i=30097&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F00%2FCC%2FZxV3aVcO-I-AAYVbALeP4manFEg2104.gz%3Ftoken%3D334254b7a083f08f646c8914a0d5f00e%26ts%3D1460598878%26attname%3DDubboM-launcher-restful.tar.gz)

选择上面其中一个下载下来之后，解压到你们机器的某个目录，阅读里面的readme.txt

##搭建DubboM项目
首先创建一个`maven`项目，并且在`pom`中添加如下依赖：

```xml
<dependency>
    <groupId>net.dubboclub</groupId>
    <artifactId>DubboM-all</artifactId>
    <version>BE-FE-1.0.1</version>
    <scope>provide</scope>
</dependency>

```

注意此时的`scope`是`provide`，因为这只是为了让代码编译通过，并不是涉及项目的打包。

接下来需要进行的就是和用`dubbo`框架开发RPC服务或者消费服务一样以及应用自身的业务开发。

###依赖dubbom的插件

为了更加便捷的进行`DubboM`的开发，这边提供了一个maven的插件。需要在你的项目pom中添加如下信息

```xml
<build>
        <plugins>
            <plugin>
                <groupId>net.dubboclub.maven</groupId>
                <artifactId>dubbom-maven-plugin</artifactId>
                <version>1.0.5</version>
                <configuration>
                		<!--配置dubbom的配置文件，该配置文件的配置内容和dubbo.properties一样,默认是classpath下面的dubbom.properties-->
                    <dubbomProperties>dubbo.properties</dubbomProperties>
                    <!--配置spring相关xml的配置文件路径，默认是classpath*:META-INF/spring/*.xml-->
                    <springXMLPath>classpath*:META-INF/spring/*.xml</springXMLPath>
                    <!--项目输出目录，默认是在项目的target下的项目名下(target/${projectname})-->
                    <targetDir>/User/bieber/dubbom-provider</targetDir>
                </configuration>
            </plugin>
        </plugins>
</build>
```
注意：上面添加的插件依赖，必须是添加在你`maven`项目的主模块里面。

同时在maven的`setting.xml`中添加相关配置

```xml
<setting>
......
 <pluginGroups>
    <pluginGroup>net.dubboclub.maven</pluginGroup>
  </pluginGroups>
......
</setting>
```

依赖这些便完成了对`DubboM`项目开发所需要准备的信息。

##对DubboM项目进行打包
通过上面依赖`dubbom`的maven插件，可以通过运行该插件来进行打包。

通过执行`mvn dubbom:pack`，那么就会把项目编译后的jar输出在配置的`targetDir`下面

##对DubboM项目开发调试
同样通过`dubbom`的maven插件，也可以进行开发调试

执行`mvn dubbom:run`

##对DubboM项目发布部署

项目的发布比较简单，只需要将上面执行打包的输出在`targetDir`下面的`xxx.tar.gz`拷贝到`DubboM-launcher`(后面成为`${DubboM-launcher-HOME}`)的`apps`下面。然后执行`${DubboM-launcher-HOME}/bin`目录下的`start.sh(bat)`则启动了该应用。

注意：在启动应用之前，请确认是否将`${DubboM-launcher-HOME}/conf/dubbom.properties`文件配置完毕(详细请求阅读${DubboM-launcher-HOME}/readme.txt文件)。

这里有一个`DubboM`的demo的地址：https://git.oschina.net/bieber/dubbom-demo.git


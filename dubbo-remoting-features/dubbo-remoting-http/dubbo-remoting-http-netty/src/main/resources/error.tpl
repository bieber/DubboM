<html>
    <head>
        <title>${status}</title>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    </head>
    <style type="text/css">
        .head{
            background-color:firebrick;
            color:aliceblue;
            font-size: 16px;
            height: 40px;
            line-height: 40px;
        }
        .head h1,.content,.powered{
            padding-left: 10px;
        }
        .content{
            font-size: 16px;
            color:firebrick;
            font-weight: bold;
            margin:10px 0px;
        }
        .powered{
            font-size: 15px;
            font-style: italic;
        }
    </style>
    <body>
        <div class="head">
            <h1>${status}</h1>
        </div>
        <div class="content">
            ${content}
        </div>
        <div class="powered">
            @Powered by DubboM
        </div>
    </body>
</html>
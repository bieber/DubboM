package net.dubboclub.http.netty.util;

import org.apache.commons.lang.StringUtils;

import java.io.*;

/**
 * @date: 2016/3/9.
 * @author:bieber.
 * @project:dubbo-side.
 * @package:com.alibaba.dubbo.remoting.http.netty.util.
 * @version:1.0.0
 * @fix:
 * @description: 描述功能
 */
public class RendingErrorUtil {

    public static String rendingError(String status,String content){
        InputStream inputStream = RendingErrorUtil.class.getClassLoader().getResourceAsStream("error.tpl");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int offset=-1;
        try {
            while((offset=inputStream.read(buffer,0,1024))>0){
                byteArrayOutputStream.write(buffer,0,offset);
            }
            String tpl = new String(byteArrayOutputStream.toByteArray());
            tpl = StringUtils.replace(tpl,"${status}",status);
            tpl = StringUtils.replace(tpl,"${content}",content);
            return tpl;
        } catch (IOException e) {
            return "";
        }
    }

    public static String rendingException(Throwable throwable){
        StringWriter stringWriter = new StringWriter();
        throwable.printStackTrace(new PrintWriter(stringWriter));
        String content = stringWriter.toString();
        content = StringUtils.replace(content, "\t", "&nbsp;&nbsp;");
        content = StringUtils.replace(content,"\n","<br/>");
        return content;
    }



}

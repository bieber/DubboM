package net.dubboclub.http.netty.servlet;

import io.netty.buffer.ByteBuf;

import java.io.IOException;
import java.io.InputStream;

/**
 * @date: 2016/3/8.
 * @author:bieber.
 * @project:dubbo-side.
 * @package:com.alibaba.dubbo.remoting.http.netty.servlet.
 * @version:1.0.0
 * @fix:
 * @description: 描述功能
 */
public class ByteBufInputStream extends InputStream {

    private ByteBuf byteBuf;

    public ByteBufInputStream(ByteBuf byteBuf) {
        this.byteBuf = byteBuf;
    }

    @Override
    public int read() throws IOException {
        if(byteBuf.readableBytes()<=0){
            return -1;
        }
        return byteBuf.readByte();
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        int readableBytes = byteBuf.readableBytes();
        if(readableBytes<=0){
            return -1;
        }
        byteBuf.readBytes(b, off, len);
        int remainBytes = byteBuf.readableBytes();
        if(remainBytes<=0){//如果剩余的字节小于等于零，那么证明已经读完了readableBytes字节
            byteBuf.release();
            return readableBytes;
        }else{
            return len;
        }
    }
}

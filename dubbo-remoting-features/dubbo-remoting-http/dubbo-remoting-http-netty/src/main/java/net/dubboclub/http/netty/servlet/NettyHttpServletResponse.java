package net.dubboclub.http.netty.servlet;

import net.dubboclub.http.netty.ChannelHolder;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.handler.codec.http.*;
import net.dubboclub.http.netty.util.RendingErrorUtil;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.*;

/**
 * @date: 2016/3/8.
 * @author:bieber.
 * @project:dubbo-side.
 * @package:com.alibaba.dubbo.remoting.http.netty.servlet.
 * @version:1.0.0
 * @fix:
 * @description: netty的http的response
 */
public class NettyHttpServletResponse extends NettyHttpServlet implements HttpServletResponse {

    private List<Cookie> cookieList = new ArrayList<Cookie>();

    private DefaultFullHttpResponse httpResponse;

    private NettyHttpServletOutputStream outputStream;

    private ChannelHolder channelHolder;

    private Throwable throwable;

    private volatile boolean isCommit=false;

    public NettyHttpServletResponse(DefaultHttpRequest httpRequest,ChannelHolder channel) {
        cookieList.addAll(Arrays.asList(encodeCookies(httpRequest)));
        httpResponse = new DefaultFullHttpResponse(httpRequest.getProtocolVersion(), HttpResponseStatus.OK);
        this.channelHolder = channel;
        outputStream = new NettyHttpServletOutputStream(httpResponse.content());
    }

    @Override
    public void addCookie(javax.servlet.http.Cookie cookie) {
        if(cookie==null){
            throw new IllegalArgumentException("cookie is null");
        }
        cookieList.add(encodeCookie(cookie));
    }

    public void send(){
        if(isCommit){
            throw new IllegalStateException("Rresponse had commit!");
        }
        isCommit=true;
        try{
            if(httpResponse.getStatus().code()>400){
                String content = httpResponse.getStatus().toString();
                if(throwable!=null){
                    content= RendingErrorUtil.rendingException(throwable);
                }
                String htmlContent = RendingErrorUtil.rendingError(httpResponse.getStatus().toString(),content);
                httpResponse.content().writeBytes(htmlContent.getBytes());
            }
            generateHeader();
            channelHolder.channel().writeAndFlush(httpResponse).addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    //更新最后一只写时间
                    channelHolder.write();
                    //回收了内存
                    assert httpResponse.refCnt()==0;
                }
            });
        }finally {
            assert httpResponse.refCnt()==1;
        }
    }

    private void generateHeader(){
        httpResponse.headers().add("version",httpResponse.getProtocolVersion().text());
        httpResponse.headers().add("status",httpResponse.getStatus().toString());
        httpResponse.headers().add(HttpHeaders.Names.DATE,new Date());
        httpResponse.headers().add(HttpHeaders.Names.SERVER,"DubboM-server");
        httpResponse.headers().add(HttpHeaders.Names.CONTENT_LENGTH,httpResponse.content().readableBytes());
        httpResponse.headers().add(HttpHeaders.Names.LAST_MODIFIED,new Date());
    }

    @Override
    public boolean containsHeader(String s) {
        return httpResponse.headers().contains(s);
    }

    @Override
    public String encodeURL(String s) {
       throw reject();
    }

    @Override
    public String encodeRedirectURL(String s) {
        throw reject();
    }

    @Override
    public String encodeUrl(String s) {
        throw reject();
    }

    @Override
    public String encodeRedirectUrl(String s) {
        throw reject();
    }

    @Override
    public void sendError(int i, String s) throws IOException {
        httpResponse.setStatus(HttpResponseStatus.valueOf(i));
        httpResponse.content().writeBytes(s.getBytes());
    }

    @Override
    public void sendError(int i) throws IOException {
        httpResponse.setStatus(HttpResponseStatus.valueOf(i));
    }

    public void exception(Throwable throwable){
        httpResponse.setStatus(HttpResponseStatus.INTERNAL_SERVER_ERROR);
        this.throwable=throwable;
    }

    @Override
    public void sendRedirect(String s) throws IOException {
        throw reject();
    }

    @Override
    public void setDateHeader(String s, long l) {
        throw reject();
    }

    @Override
    public void addDateHeader(String s, long l) {
        throw reject();
    }

    @Override
    public void setHeader(String s, String s1) {
        httpResponse.headers().set(s,s1);
    }

    @Override
    public void addHeader(String s, String s1) {
        httpResponse.headers().add(s,s1);
    }

    @Override
    public void setIntHeader(String s, int i) {
        httpResponse.headers().set(s,String.valueOf(i));
    }

    @Override
    public void addIntHeader(String s, int i) {
        httpResponse.headers().add(s,String.valueOf(i));
    }

    @Override
    public void setStatus(int i) {
        httpResponse.setStatus(HttpResponseStatus.valueOf(i));
    }

    @Override
    public void setStatus(int i, String s) {
        httpResponse.setStatus(HttpResponseStatus.valueOf(i));
    }

    @Override
    public int getStatus() {
        return httpResponse.getStatus().code();
    }

    @Override
    public String getHeader(String s) {
        return httpResponse.headers().get(s);
    }

    @Override
    public Collection<String> getHeaders(String s) {
        return httpResponse.headers().getAll(s);
    }

    @Override
    public Collection<String> getHeaderNames() {
        return httpResponse.headers().names();
    }

    @Override
    public String getCharacterEncoding() {
        return httpResponse.headers().get(HttpHeaders.Names.CONTENT_ENCODING);
    }

    @Override
    public String getContentType() {
        return httpResponse.headers().get(HttpHeaders.Names.CONTENT_TYPE);
    }

    @Override
    public ServletOutputStream getOutputStream() throws IOException {
        return outputStream;
    }

    @Override
    public PrintWriter getWriter() throws IOException {
        return new PrintWriter(new OutputStreamWriter(getOutputStream()));
    }

    @Override
    public void setCharacterEncoding(String s) {
        httpResponse.headers().set(HttpHeaders.Names.CONTENT_ENCODING,s);
    }

    @Override
    public void setContentLength(int i) {
        httpResponse.headers().set(HttpHeaders.Names.CONTENT_LENGTH,i);
    }

    @Override
    public void setContentType(String s) {
        httpResponse.headers().set(HttpHeaders.Names.CONTENT_TYPE,s);
    }

    @Override
    public void setBufferSize(int i) {
        throw reject();
    }

    @Override
    public int getBufferSize() {
        throw reject();
    }

    @Override
    public void flushBuffer() throws IOException {
        throw reject();
    }

    @Override
    public void resetBuffer() {
        throw reject();
    }

    @Override
    public boolean isCommitted() {
        return isCommit;
    }

    @Override
    public void reset() {
        throw reject();
    }

    @Override
    public void setLocale(Locale locale) {
        throw reject();
    }

    @Override
    public Locale getLocale() {
        throw reject();
    }
}

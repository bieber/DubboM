package net.dubboclub.http.netty;

import com.alibaba.dubbo.common.URL;
import com.alibaba.dubbo.remoting.http.HttpBinder;
import com.alibaba.dubbo.remoting.http.HttpHandler;
import com.alibaba.dubbo.remoting.http.HttpServer;

/**
 * @date: 2016/3/7.
 * @author:bieber.
 * @project:dubbo-side.
 * @package:com.alibaba.dubbo.remoting.http.netty.
 * @version:1.0.0
 * @fix:
 * @description: 描述功能
 */
public class NettyHttpBinder implements HttpBinder {
    @Override
    public HttpServer bind(URL url, HttpHandler handler) {
        return new NettyHttpServer(url,handler);
    }
}

package net.dubboclub.restful.export.mapping;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import net.dubboclub.restful.modal.RestfulPackage;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @date: 2016/2/22.
 * @author:bieber.
 * @project:dubbo-plus.
 * @package:net.dubboclub.restful.export.mapping.
 * @version:1.0.0
 * @fix:
 * @description: 请求的实体
 */
public class RequestEntity implements Serializable {


    private final  static Pattern argPattern = Pattern.compile("^(arg[1-9]{1}[0-9]{0,})$");
    private final  static Pattern argIndexPattern = Pattern.compile("[1-9]{1}[0-9]{0,}");

    private String group;

    private String version;

    private String method;

    public RequestEntity(){

    }

    public RequestEntity(JSONObject jsonObject){
        if(jsonObject!=null){
            this.setGroup(jsonObject.getString("group"));
            this.setVersion(jsonObject.getString("version"));
            this.setMethod(jsonObject.getString("method"));
        }
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }


    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    @Override
    public String toString() {
        return "RequestEntity{" +
                "group='" + group + '\'' +
                ", version='" + version + '\'' +
                ", method='" + method + '\'' +
                '}';
    }
}

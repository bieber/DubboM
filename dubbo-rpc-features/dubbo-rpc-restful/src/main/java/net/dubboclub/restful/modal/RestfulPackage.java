package net.dubboclub.restful.modal;


import java.io.Serializable;

/**
 * Created by bieber on 16/3/10.
 * restful的接口基础类
 * restful的接口入参必须是这个类的之类,否则不进行发布以及调用
 */
public class RestfulPackage implements Serializable{

    //客户端id
    private String clientId;

    //请求的方法名
    private String method;

    //请求的服务版本号
    private String version;

    //请求的服务分组
    private String group;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}

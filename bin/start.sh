#!/bin/bash
DUBBOM_HOME=$(cd "$(dirname "$0")"; pwd)
CONFDIR="${DUBBOM_HOME}/../conf"
LOG_HOME="${DUBBOM_HOME}/../logs"
CLASSPATH="${DUBBOM_HOME}/../libs/*:${CONFDIR}"
MAINCLASS="net.dubboclub.dubbom.launcher.Launcher"


if [ -z $JAVA_HOME ]; then
	JAVA="java"
else
	JAVA="$JAVA_HOME/bin/java"
fi


DUBBOM_PROPERTIES="dubbom.properties"

"$JAVA" "-Ddubbom.log.home=${LOG_HOME}" "-Ddubbo.properties.file=${DUBBOM_PROPERTIES}" "-Dwork.dir=${DUBBOM_HOME}/../apps/" -cp $CLASSPATH "${MAINCLASS}" start
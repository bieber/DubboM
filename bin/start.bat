setlocal
SET CONFDIR=%~dp0%..\conf
SET LOG_HOME=%~dp0%..\logs
SET CP=%~dp0..\libs\*;%CONFDIR%
SET WORK_DIR=%~dp0..\apps\
set MAINCLASS=net.dubboclub.dubbom.launcher.Launcher

SET DUBBO_PROPERTIES=dubbom.properties
echo on
java "-Ddubbom.log.home=%LOG_HOME%" "-Ddubbo.properties.file=%DUBBO_PROPERTIES%" "-Dwork.dir=%WORK_DIR%" -cp "%CP%" "%MAINCLASS%" start
endlocal

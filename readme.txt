启动DubboM-launcher步骤

1.确保你当前环境已经安装好了java环境(1.7+)

2.你可以配置你的项目

在conf目录下对dubbom.properties进行配置可以调整相关参数

dubbo.application.name:配置当前应用名
dubbo.application.owner:配置当前应用所属人
dubbo.protocol.contextpath:当前应用的上下文路径,如果当前应用是服务提供者,那么将会是服务请求路径上的上下文(默认是/)
dubbom.spring.xml:spring的xml文件路径,多个通过英文逗号(,)隔开
dubbo.protocol.port:配置当前应用发布服务的端口
dubbo.protocol.name:配置当前应用的协议名(当前dubbom支持dubbo和restful两种协议)

注意,如果你的应用暴露多种协议,则不能再此处配置达到目的,而是在dubbo的xml文件里面通过配置多个<dubbo:protocol...

例如:

<dubbo:protocol name="dubbo" port="xxxx" contextpath="xxxx"/>
<dubbo:protocol name="restful" port="xxxx" contextpath="xxxx"/>

这部分内容和原生dubbo一样,并没有做任何变化

dubbo.registry.address:配置当前注册中心,如果当前应用是消费端,那么这个地址必须配置,如果这个地址是提供端,这个地址则可以不进行配置

不配置则表示,你将服务发布到注册中心